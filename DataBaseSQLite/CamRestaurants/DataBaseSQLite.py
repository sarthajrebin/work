###############################################################################
# PyDial: Multi-domain Statistical Spoken Dialogue System Software
###############################################################################
#
# Copyright 2015 - 2017
# Cambridge University Engineering Department Dialogue Systems Group
#
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################

'''
DataBaseSQLite.py - loads sqlite db  
====================================================

Copyright CUED Dialogue Systems Group 2015 - 2017

Uses SQL style queries to access database entities. Note with sqlite3 approach, database is just a file (ie no other program
needs to be running, like would be required for mongodb for example). 

.. Note::
    Called by :mod:`utils.Settings` to load database into global variable db

.. seealso:: CUED Imports/Dependencies: 

    import :mod:`utils.ContextLogger` |.|
    import :mod:`ontology.DataBase` |.|
    import :mod:`utils.Settings` |.|

************************

'''

__author__ = "cued_dialogue_systems_group"
import sqlite3
from utils import Settings
from DataBase import DataBaseINTERFACE
from utils import ContextLogger
from test_api import sample_API_Call

logger = ContextLogger.getLogger('')


# Note utility function get_dist(c1, c2): in DataBase.py if new ontologies are added and (lattitude, longitude) pairs need
# translating into area bins.

class DataBase_SQLite(DataBaseINTERFACE):
    '''SQLite3 access to entities. No explicit schema info here-- See scripts file script_txt2JSON_or_SQLITE.py which was used
    to create databases (domainTag-dbase.db in ontology/ontologies).
    -- basically: name is the primary key, all requestable slots are columns. 
    -- No other rules are added in SQL database regarding other lookups for commonly searched columns. Can use sql for this to optimise
    further if desired. 
    '''
    def __init__(self, dbfile, dstring):
        self._loaddb(dbfile)
        self.domain = dstring
        self.flag = 1
        self.restaurants = {}
        self.first = True
        self.oldFood = None
        self.oldPricerange = None
        self.oldPricerange = None
        self.no_constraints_sql_query = '''select  * 
                from {}'''.format(self.domain) 
                
        self.limit = 10 # number of randomly returend entities
    
    def _loaddb(self, dbfile):
        '''Sets self.db
        '''
        #print "HERE"
        try:
            self.db_connection = sqlite3.connect(dbfile)
            self.db_connection.row_factory = self._dict_factory   # for getting entities back as python dict's
            self.cursor = self.db_connection.cursor()         # we will just run 1 query - so only need a single cursor object
        except Exception as e:
            print e
            logger.error('Could not load database file: %s' % dbfile)
        return
    
    def _dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d
    
    def entity_by_features(self, constraints):
        '''Retrieves from database all entities matching the given constraints. 
       
        :param constraints: features. Dict {slot:value, ...} or List [(slot, op, value), ...] \
        (NB. the tuples in the list are actually a :class:`dact` instances)
        :returns: (list) all entities (each a dict)  matching the given features.
        '''
        
        # 1. Format constraints into sql_query 
        # NO safety checking - constraints should be a list or a dict 
        # Also no checking of values regarding none:   if const.val == [None, '**NONE**']: --> ERROR
        doRand = False


        ### EDITED ###

        # Try for printing constraints only 1 time per turn. (Current Status: Not successful)

        if bool(constraints):
            print "CONSTRAINTS"
            print constraints

        ### EDITED ###
        
        
        if len(constraints):
            bits = []
            values = []
            if isinstance(constraints, list):
                for const in constraints:
                    #print "CONST PRINTING"
                    #print const
                    if const.op == '=' and const.val == 'dontcare':
                        continue       # NB assume no != 'dontcare' case occurs - so not handling
                    if const.op == '!=' and const.val != 'dontcare':
                        bits.append(const.slot +'!= ?')
                    else:
                        bits.append(const.slot +'= ?  COLLATE NOCASE')
                    values.append(const.val)
            elif isinstance(constraints, dict):
                for slot,value in constraints.iteritems():
                    #print "SLOT VALUE PRINTING"
                    #print slot, value
                    if value != 'dontcare':
                        bits.append(slot +'= ? COLLATE NOCASE')
                        values.append(value)

                    #print "PRINTING VALUES"
                    #print values
                    #print "PRINTING BITS"
                    #print bits

            # 2. Finalise and Execute sql_query
            try:
                if len(bits):
                    print "PRINTING DOMAIN"
                    print self.domain

                    ### EDITED for CamRestaurants###

                    if self.domain == 'CamRestaurants':

                        if not all(k in constraints for k in ('food', 'area', 'pricerange')):
                            if self.flag != 0:
                                print "NOT FULL"
                                sample_result = [{'addr': u'jp nagar', 'area': u'bangalore', 'food': u'chinese', 'description': u'not available', 'phone': u'789', 'pricerange': u'moderate', 'postcode': u'45', 'signature': u'not available', 'id': u'456123', 'name': u'KAi ruchi'}, {'addr': u'33 bridge street', 'area': u'centre', 'food': u'european', 'description': u'', 'phone': u'01223 362054', 'pricerange': u'moderate', 'postcode': u'c.b 2, 1 u.w', 'signature': u'poached fillets of monkfish in lemongrass with sweet red chilli cream sauce and tiger prawns with leeks and mushrooms served with rice', 'id': u'6780', 'name': u'galleria'}, {'addr': u'7 milton road chesterton', 'area': u'north', 'food': u'indian', 'description': u'not available', 'phone': u'01223 360966', 'pricerange': u'moderate', 'postcode': u'c.b 4, 1 u.y', 'signature': u'not available', 'id': u'19263', 'name': u'the nirala'}, {'addr': u'84 regent street city centre', 'area': u'centre', 'food': u'mediterranean', 'description': u'shiraz serves traditional mediterranean cuisine accompanied by an extensive wine list with beers and spirits also available', 'phone': u'01223 307581', 'pricerange': u'expensive', 'postcode': u'c.b 2, 1 d.p', 'signature': u'not available', 'id': u'19217', 'name': u'shiraz restaurant'}, {'addr': u'1 kings parade', 'area': u'centre', 'food': u'british', 'description': u'', 'phone': u'01223 359506', 'pricerange': u'expensive', 'postcode': u'c.b 2, 1 s.j', 'signature': u'lamb barnsley chop potato and garlic bake greens and gravy', 'id': u'6941', 'name': u'the cambridge chop house'}, {'addr': u'cambridge lodge hotel 139 huntingdon road city centre', 'area': u'west', 'food': u'european', 'description': u'located in the cambridge lodge hotel, this restaurant serves a variety of european and vegetarian dishes and can cater for private parties of up to 20 guests upon request.', 'phone': u'01223 355166', 'pricerange': u'expensive', 'postcode': u'c.b 3, 0 d.q', 'signature': u'not available', 'id': u'19252', 'name': u'cambridge lodge restaurant'}, {'addr': u'10 homerton street city centre', 'area': u'south', 'food': u'chinese', 'description': u'peking resturant cook from fresh ingredients. they specialise in sichuan and hunan dishes', 'phone': u'01223 354755', 'pricerange': u'expensive', 'postcode': u'c.b 2, 8 n.x', 'signature': u'not available', 'id': u'19246', 'name': u'peking restaurant'}, {'addr': u'mill road city centre', 'area': u'centre', 'food': u'indian', 'description': u'the golden curry serves a variety of authentic indian dishes at their fully licensed restaurant', 'phone': u'01223 329432', 'pricerange': u'expensive', 'postcode': u'c.b 1, 2 a.z', 'signature': u'not available', 'id': u'19182', 'name': u'the golden curry'}, {'addr': u'40270 king street city centre', 'area': u'centre', 'food': u'modern european', 'description': u'darrys cookhouse and wine shop is an award winning drinking and dining restaurant and bar in the centre of cambridge', 'phone': u'01223 505015', 'pricerange': u'expensive', 'postcode': u'c.b 1, 1 l.n', 'signature': u'not available', 'id': u'19177', 'name': u'darrys cookhouse and wine shop'}, {'addr': u'290 mill road city centre', 'area': u'east', 'food': u'gastropub', 'description': u'not available', 'phone': u'01223 247877', 'pricerange': u'expensive', 'postcode': u'c.b 1, 3 n.l', 'signature': u'not available', 'id': u'19190', 'name': u'royal standard'}]
                                return sample_result


                        if all(k in constraints for k in ('food', 'area', 'pricerange')):
                            if self.first == True:
                                self.oldFood = constraints['food']
                                self.oldArea = constraints['area']
                                self.oldPricerange = constraints['pricerange']
                                self.first = False

                            if (constraints['food'] != self.oldFood or constraints['area'] != self.oldArea or constraints['pricerange'] != self.oldPricerange):
                                print "HERE"
                                self.flag = 1
                                print "FLAG:",self.flag

                            #print "NORTH REMEMBERS"
                            #print constraints
                            if self.flag != 0:
                                print "HAI"

                                if constraints['food']:
                                    print "HELL YAA!!!"
                                else:
                                    print "HELL NO"

                                #city = 'bangalore'
                                city = constraints['area']
                                #print city
                                if constraints['pricerange'] == 'cheap':
                                    price = 1
                                elif constraints['pricerange'] == 'moderate':
                                    price = 2
                                else:
                                    price = 3
                                self.oldFood = constraints['food']
                                self.oldArea = constraints['area']
                                self.oldPricerange = constraints['pricerange']
                                self.restaurants = sample_API_Call(city,constraints['food'],price)
                                self.flag = 0
                                #print self.restaurants
                                return self.restaurants

                        if self.flag == 0:
                            if 'name' in constraints:
                                print "HI"
                                for i in range(len(self.restaurants)):
                                    print len(self.restaurants)
                                    print constraints['name']
                                    print self.restaurants[i]['name']
                                    if self.restaurants[i]['name'].lower() == constraints['name'].lower():
                                        print "NAME2"
                                        print self.restaurants[i]
                                        restau_list = []
                                        restau_list.append(self.restaurants[i])
                                        print restau_list
                                        return restau_list
                            else:
                                return self.restaurants


                    ### EDITED ###

                    sql_query = '''select  *
                    from {} 
                    where '''.format(self.domain)
                    #print "SQL :",sql_query
                    sql_query += ' and '.join(bits)
                    c = self.cursor.execute(sql_query, tuple(values))
                else:
                    sql_query =  self.no_constraints_sql_query
                    self.cursor.execute(sql_query)
                    doRand = True
            except Exception as e:
                print e     # hold to debug here
                logger.error('sql error ' + str(e))
            #print "sql_query:",sql_query
                
                
            
        else:
            # NO CONSTRAINTS --> get all entities in database?  
            #TODO check when this occurs ... is it better to return a single, random entity? --> returning random 10
            
            # 2. Finalise and Execute sql_query
            sql_query =  self.no_constraints_sql_query
            #print "SQL QUERY"
            #print sql_query
            self.cursor.execute(sql_query)
            doRand = True
        
        results = self.cursor.fetchall()
        #print "PRINTING RESULTS"# can return directly
        #print results

        
        if doRand:
            Settings.random.shuffle(results)
            if len(results) > self.limit:
                results = results[:self.limit]
        #print "RESULTS"
        #print results
        return results
    
    def query_entity_property(self, name, slot, value):
        '''
        '''
        #print "HERE"
        sql_query = '''select  * 
                from {} 
                where name="{}" COLLATE NOCASE'''.format(self.domain,name)
        # 2. Execute SQL query
        self.cursor.execute(sql_query)
        results = self.cursor.fetchall()        # TODO delete -- just for debug
        assert(len(results)==1)  # name should be a unique identifier
        try:
            if results[0][slot] == value:
                return True
        except:
            pass        # some entities haven't had all values for domains slots filled in - although I think this is fixed
        return False
    
    def get_all_entities(self):
        sql_query = ''' select * from {}'''.format(self.domain)
        self.cursor.execute(sql_query)
        results = self.cursor.fetchall()
        return results
    
    def get_num_unique_entities(self, cols = None):
        #print "HERE"
        colStatement = '*'
        if cols is not None:
            colStatement = ','.join(cols)
        sql_query = ''' SELECT count(*) from (select distinct {} from {})'''.format(colStatement,self.domain)
        self.cursor.execute(sql_query)
        results = self.cursor.fetchall()
        return results[0]['count(*)']
    
    def _customQuery(self, query):
        self.cursor.execute(query)
        results = self.cursor.fetchall()
        return results
    
    
    
# END OF FILE
