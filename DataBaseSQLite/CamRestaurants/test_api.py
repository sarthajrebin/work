import requests,json

def sample_API_Call(city,food,price):

	headrs = {"user-key": '4728a2959fbce7303b7b896825d36363'}

	city_params = {"q": city}
	city_id_response = requests.get("https://developers.zomato.com/api/v2.1/cities", headers=headrs, params=city_params)

	data = city_id_response.json()
	
	city_id = data['location_suggestions'][0]['id']
	#state_name = data['location_suggestions'][0]['state_name']

	#print city_id 

	cuisine_params = {"city_id": city_id}
	cuisine_id_response = requests.get("https://developers.zomato.com/api/v2.1/cuisines", headers=headrs, params=cuisine_params)
	cuisine_data = cuisine_id_response.json()
	#print cuisine_data

	cuisine_flag = 0
	for id in cuisine_data['cuisines']:
		#print id
		if id['cuisine']['cuisine_name'].lower() == food:
			#print 'found'
			print id['cuisine']['cuisine_name'].lower()
			print food
			cuisine_id = id['cuisine']['cuisine_id']
			cuisine_flag = 1
			#print cuisine_id
	if cuisine_flag == 0:
		print "Cuisine not Found"

	search_params = {"entity_id": city_id, "entity_type": 'city', "cuisines": cuisine_id }
	search_response = requests.get("https://developers.zomato.com/api/v2.1/search", headers=headrs, params=search_params)

	search_data = search_response.json()
	#print city_id,cuisine_id
	#print search_data
	
	sample_dict = []
	count = 0
	#print(response.content)
	for restaurant in search_data["restaurants"]:
		count += 1
		#print restaurant
		addr = restaurant["restaurant"]["location"]["address"]
		area = restaurant["restaurant"]["location"]["city"]
		food = restaurant["restaurant"]["cuisines"]
		description = restaurant["restaurant"]["url"]

		if 'phone_numbers' in search_response.content:
			phone = restaurant["restaurant"]["phone_numbers"]
		else:
			phone = "not available"
		#pricerange = restaurant["restaurant"]["price_range"]
		pricerang = restaurant["restaurant"]["price_range"]
		
		if pricerang == 1:
			pricerange = "cheap"	#Hard coded now. TO BE changed with original DB
		elif pricerang == 2:
			pricerange = "moderate"
		else:
			pricerange = "expensive"

		postcode = restaurant["restaurant"]["location"]["zipcode"]
		signature = "not available"
		id1 = restaurant["restaurant"]["id"]
		name = restaurant["restaurant"]["name"]
		
		for i in restaurant:
			if pricerang == price:
				s = {'addr': addr, 'area': area, 'food': food, 'description': description, 'phone': phone, 'pricerange': pricerange, 'postcode': postcode, 'signature': signature, 'id': id1, 'name': name}
				#print s
				sample_dict.append(s)

	#print sample_dict


	return sample_dict
	
	#print(response.content)
	
#sample_API_Call('birmingham','indian',2)